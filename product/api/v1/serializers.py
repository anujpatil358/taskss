from rest_framework import serializers
from product.models import Product


class ProductSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if not data['price'] > 10 and data['price'] < 5000:
            raise serializers.ValidationError("price should be greater than 10 and less than 5000")
        return data

    class Meta:
        model = Product
        fields = ['id', 'category', 'title', 'summary', 'price', ]
        depth = 1

