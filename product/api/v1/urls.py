from django.urls import path, include
from .views import ProductList

urlpatterns = [
    path('product/', ProductList.as_view(), name='product-list'),
]

