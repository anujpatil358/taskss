from django.db import models


class Category(models.Model):
    name = models.TextField()


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    summary = models.TextField(max_length=1000, help_text='Enter a brief description of the Product')
    price = models.FloatField(default=0.0)
