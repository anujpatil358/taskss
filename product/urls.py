from django.urls import path, include
from .views import ProductView

urlpatterns = [
	path('', ProductView.as_view(), name='product-add'),
	path('api/', include('product.api.v1.urls')),
]
