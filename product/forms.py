from django import forms
from .models import Product


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['title', 'summary', 'price']

    # def clean_price(self):
    #     price = self.cleaned_data.get('price')
    #     if not price > 10 and price < 5000:
    #         return "price should be greater than 10 and less than 5000: "
	# 	return price
